# dataview
vue + echarts简单的数据大屏项目

目前适配的是1920*1080，其他分辨率的话可能要微调下中间的位置~

![image](https://www.tootootool.com/wp-content/uploads/2022/06/img-1024x657.gif)

demo:
http://erhuo.tootootool.com/dataview/index.html


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
