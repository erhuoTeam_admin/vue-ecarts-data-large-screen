module.exports = {
  publicPath: "./",
  devServer: {
    disableHostCheck: false,
    host: "127.0.0.1",
    port: 8099,
    https: false,
    hotOnly: false,
    proxy: null,
  },
};
